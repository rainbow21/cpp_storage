#ifndef IVLIST_H
#define IVLIST_H
#include "ivnode.h"
#include <stddef.h>
typedef unsigned char byte;
// ����� ���������� ��������
#define LIST_NEXT_PTR(element, Type, offset) (*(Type**)(((byte*)element) + offset))
// �������� ������
#define ivList(Type, next) ivListBasis<Type, (int)&(((Type*)NULL)->next)>

template <class Type, int offset>
class ivListBasis
{
private:
	Type*	begin;											// ��������� �� ������ ������ 
	Type*	end;											// ��������� �� ����� ������			
	int		num;											// ���������� ���������

	void	append(Type *element);							// �������� ��������� �� �������
	void	insert(Type *element, int index);				// �������� ��������� �� ������� �� ��������� �������
	
public:
			ivListBasis();									// ����������� �� ���������
			ivListBasis(const ivListBasis<Type, offset> &x);// ����������� �����������
			~ivListBasis();									// ���������� �� ���������

	void	append(const Type &x);							// �������� ������� �� ��������
	void	insert(const Type &x, int index = 0);			// �������� ������� �� ��������� ������� �� ��������
	Type*	cut(int index = 0);								// �������� ������� �� ��������� �������, ��������� ������ � ������ ���������.
	int		getNum() const;									// ���������� ���������
	void	clear();										// �������� ������

	Type*	getElement(int i) const;						// �������� ������ � ��������
	Type*	operator[] (int i) const;						// �������� ������ � ��������
    ivListBasis<Type, offset>&   operator=(const ivListBasis<Type, offset> &lst);
};


// ivListBasis()
template <class Type, int offset>
ivListBasis<Type, offset>::ivListBasis()
{
	begin = end = NULL;
	num = 0;
}

// ivListBasis(const ivList<Type, offset> &x)
template <class Type, int offset>
ivListBasis<Type, offset>::ivListBasis(const ivListBasis<Type, offset> &x)
{
	begin = end = NULL;
	num = 0;
	for (int i = 0; i < x.getNum(); i++)
	{
		Type newElement = *x[i];
		append(newElement);
	}
}

// ~ivListBasis()
template <class Type, int offset>
ivListBasis<Type, offset>::~ivListBasis()
{
    while (num)
        delete cut();

}

// append(Type *element)
template <class Type, int offset>
void ivListBasis<Type, offset>::append(Type *element)
{
	if (!element)
		return;
	if (end)
		LIST_NEXT_PTR(end, Type, offset) = element;
	end = element;
	if (!begin)
		begin = element;
	num++;
}

// insert(Type *element, int index)
template <class Type, int offset>
void ivListBasis<Type, offset>::insert(Type *element, int index)
{
	if (index < 0)
		return;
	if (index > num)
		index = num;

	if (index == num)
	{
		append(element);
		return;
	}

	if (index == 0)
	{
		LIST_NEXT_PTR(element, Type, offset) = begin;
		begin = element;
		num++;
		return;
	}
	
	Type *cur = begin;
	for (int i = 0; i < index-1; i++)
		cur = LIST_NEXT_PTR(cur, Type, offset);
	// ������ cur ��������� �� �������, ������������� ����� ������ �������
	LIST_NEXT_PTR(element, Type, offset) = LIST_NEXT_PTR(cur, Type, offset);
	LIST_NEXT_PTR(cur, Type, offset) = element;
	
	num++;
}

// append(const Type &x)
template <class Type, int offset>
void ivListBasis<Type, offset>::append(const Type &x)
{
	Type *element = new Type(x);
	LIST_NEXT_PTR(element, Type, offset) = NULL;
	append(element);
}

// insert(const Type &x, int index)
template <class Type, int offset>
void ivListBasis<Type, offset>::insert(const Type &x, int index)
{
	Type *element = new Type(x);
	insert(element, index);
}

// cut(int index = 0)
template <class Type, int offset>
Type* ivListBasis<Type, offset>::cut(int index)
{
	if (index < 0 || index >= num)
		return NULL;

	if (index == 0)
	{
        Type *result = begin;
        if (num == 1)
            begin = end = NULL;
        else
            begin = LIST_NEXT_PTR(begin, Type, offset);

        num--;
		return result;
	}

	Type *cur = begin;
	for (int i = 0; i < (index-1); i++)
		cur = LIST_NEXT_PTR(cur, Type, offset);


	Type *result = LIST_NEXT_PTR(cur, Type, offset);
	LIST_NEXT_PTR(cur, Type, offset) = LIST_NEXT_PTR(result, Type, offset);

	num--;
	return result;
}


// getNum()
template <class Type, int offset>
int ivListBasis<Type, offset>::getNum() const
{
	return num;
}

// clear()
template <class Type, int offset>
void ivListBasis<Type, offset>::clear()
{
	while (num)
		delete cut();
}

// getElement(int i)
template <class Type, int offset>
Type* ivListBasis<Type, offset>::getElement(int i) const
{
	if (i < 0 || i >= num) return NULL;
	
	Type *cur = begin;
	for (int j = 0; j < i; j++)
		if (cur)
			cur = LIST_NEXT_PTR(cur, Type, offset);

	return cur;
}

// operator[](int i)
template <class Type, int offset>
Type* ivListBasis<Type, offset>::operator[](int i) const
{
	return getElement(i);
}

// operator=
template <class Type, int offset>
ivListBasis<Type, offset>& ivListBasis<Type, offset>::operator =(const ivListBasis<Type, offset> &lst)
{
    for (int i =0; i < lst.getNum(); i++)
        this->append(*lst[i]);
    return *this;
}

#endif
